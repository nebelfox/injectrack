from dataclasses import dataclass

from geometry import Vec2


@dataclass
class Insertion:
    offset: Vec2
    timestamp: int

    @staticmethod
    def from_dict(data: dict):
        offset = Vec2(data['offset_x'], data['offset_y'])
        return Insertion(
            offset,
            data['timestamp']
        )

    def to_dict(self) -> dict:
        return {
            'offset_x': self.offset.x,
            'offset_y': self.offset.y,
            'timestamp': self.timestamp
        }
