from dataclasses import dataclass
from typing import List
from datetime import datetime

from .insertion import Insertion
from geometry import Vec2


@dataclass
class Cycle:
    center: Vec2
    radii: Vec2
    insertions: List[Insertion]

    def latest_insertion(self) -> Insertion:
        return max(self.insertions, key=lambda insertion: insertion.timestamp)

    def oldest_insertion(self) -> Insertion:
        return min(self.insertions, key=lambda insertion: insertion.timestamp)

    def latest_timestamp(self) -> int:
        return self.latest_insertion().timestamp

    def oldest_timestamp(self) -> int:
        return self.oldest_insertion().timestamp


    def append(self, pos: Vec2) -> Insertion:
        offset = pos - self.center
        timestamp = int(datetime.utcnow().timestamp())
        insertion = Insertion(offset, timestamp)
        self.insertions.append(insertion)
        return insertion

    @staticmethod
    def from_dict(data: dict):
        center = Vec2(data['x'], data['y'])
        radii = Vec2(data['radius_x'], data['radius_y'])
        return Cycle(
            center,
            radii,
            [Insertion.from_dict(insertion) for insertion in data['insertions']]
        )

    def to_dict(self) -> dict:
        return {
            'x': self.center.x,
            'y': self.center.y,
            'radius_x': self.radii.x,
            'radius_y': self.radii.y,
            'insertions': [insertion.to_dict() for insertion in self.insertions]
        }
