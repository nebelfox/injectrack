from dataclasses import dataclass
from .vec2 import Vec2


@dataclass
class Plane:
    origin: Vec2
    unit: float

    def localize(self, point: Vec2) -> Vec2:
        x = (point.x - self.origin.x) / self.unit
        y = (point.y - self.origin.y) / self.unit
        return Vec2(x, y)

    def globalize(self, point: Vec2) -> Vec2:
        x = point.x * self.unit + self.origin.x
        y = point.y * self.unit + self.origin.y
        return Vec2(x, y)
