from dataclasses import dataclass

from geometry import Vec2


@dataclass
class Rect:
    top_left: Vec2
    bottom_right: Vec2

    @property
    def top_right(self) -> Vec2:
        return Vec2(self.bottom_right.x, self.top_left.y)

    @property
    def bottom_left(self) -> Vec2:
        return Vec2(self.top_left.x, self.bottom_right.y)

    @property
    def top(self) -> float:
        return self.top_left.y

    @property
    def bottom(self) -> float:
        return self.bottom_right.y

    @property
    def left(self) -> float:
        return self.top_left.x

    @property
    def right(self) -> float:
        return self.bottom_right.x

    @property
    def center(self) -> Vec2:
        x = self.top_left.x + (self.bottom_right.x - self.top_left.x)*.5
        y = self.top_left.y + (self.bottom_right.y - self.top_left.y) * .5
        return Vec2(x, y)

    @property
    def width(self) -> float:
        return self.bottom_right.x - self.top_left.x

    @property
    def height(self) -> float:
        return self.bottom_right.y - self.top_left.y