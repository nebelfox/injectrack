from dataclasses import dataclass
from math import sqrt


@dataclass
class Vec2:
    x: float
    y: float

    @property
    def ints(self):
        return Vec2(int(self.x), int(self.y))

    @property
    def tuple(self):
        return self.x, self.y

    def distance(self, other) -> float:
        dx = self.x - other.x
        dy = self.y - other.y
        return sqrt(dx * dx + dy * dy)

    def __add__(self, other):
        return Vec2(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Vec2(self.x - other.x, self.y - other.y)
