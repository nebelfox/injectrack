import cv2
import math

from geometry import Vec2, Rect, Plane
from history import History
from yolo_utils import load_latest_model
from config import *


# load the history
history = History.load_from_json_file(HISTORY_FILE_PATH)
area = history['abdomen-right']
latest_cycle = area.get_latest_cycle()


# load the model from the latest train results
model = load_latest_model()


# set up the camera input
cap = cv2.VideoCapture(CAMERA_INDEX)
cap.set(3, CAPTURE_WIDTH)
cap.set(4, CAPTURE_HEIGHT)
show_history = True


class_names = ['inserter', 'navel']


def display_inserter(rect, img):
    radius = int((rect.width + rect.height) * 0.25)
    center = rect.center.ints.tuple
    color = (55, 55, 255)
    # contour
    cv2.circle(img, center, radius, color, 3)
    # center
    cv2.circle(img, center, 2, color, 3)


def display_navel(rect, img):
    center = rect.center
    color = (255, 0, 255)
    cv2.line(img, center.ints.tuple, (int(center.x), 0), color, 3)
    cv2.line(img, (0, int(center.y)), (CAPTURE_WIDTH, int(center.y)), color, 3)


# main loop
while True:
    success, img = cap.read()
    results = model(img, stream=True, max_det=2)

    # collect results
    rects = [None, None]
    confidences = [0, 0]

    for r in results:
        boxes = r.boxes

        for box in boxes:
            confidence = math.ceil((box.conf[0] * 100)) / 100
            cls = int(box.cls[0])
            if confidence > confidences[cls]:
                x1, y1, x2, y2 = map(float, box.xyxy[0])
                rect = Rect(Vec2(x1, y1), Vec2(x2, y2))
                rects[cls] = rect
                confidences[cls] = confidence

    inserter, navel = rects
    if navel:
        display_navel(navel, img)
    if inserter:
        display_inserter(inserter, img)

    plane, inserter_pos = None, None
    if inserter and navel:
        origin = Vec2(navel.left, navel.top).ints
        unit = (inserter.width + inserter.height) * .5
        plane = Plane(origin, unit)
        inserter_pos = plane.localize(inserter.center)

        if show_history:
            oldest_timestamp = latest_cycle.oldest_timestamp()
            latest_timestamp = latest_cycle.latest_timestamp()
            timespan = latest_timestamp - oldest_timestamp
            for insertion in latest_cycle.insertions:
                pos = insertion.offset + latest_cycle.center
                x, y = plane.globalize(pos).ints.tuple
                time_delta = insertion.timestamp - oldest_timestamp
                color = (0, 0, int(255 * (1 - time_delta / timespan)))
                cv2.circle(img, (x, y), 2, color, 3, cv2.FILLED)

                if inserter_pos.distance(pos) < MIN_DISTANCE:
                    cv2.line(img, (x, y), inserter.center.ints.tuple, (255, 0, 0),
                             3)

    cv2.imshow(WINDOW_NAME, img)

    key = cv2.waitKey(1)
    if key == EXIT_KEY:
        break
    elif key == CONFIRM_KEY:
        if navel and inserter:
            insertion = latest_cycle.append(inserter_pos)
    elif key == SHOW_HISTORY_KEY:
        show_history = not show_history
    elif key == SAVE_HISTORY_KEY:
        history.save_to_json_file(HISTORY_FILE_PATH)
    elif key == RELOAD_HISTORY_KEY:
        history = History.load_from_json_file(HISTORY_FILE_PATH)


cap.release()
cv2.destroyAllWindows()
